from CarcinogenesisPredictor import CarcinogenesisPredictor

# Provide training data turtle file
cp = CarcinogenesisPredictor('../Resources/Input/kg-mini-project-grading.ttl')
cp.fit()
# Provide testing data turtle file
cp.predict('../Resources/Input/kg-mini-project-testing.ttl')
cp.scores()

