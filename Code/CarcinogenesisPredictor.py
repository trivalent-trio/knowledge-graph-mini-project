import json
import pandas as pd
from Converter import Converter
from Scores import Scores
from ontolearn.refinement_operators import KnowledgeBase
from ontolearn.concept_learner import CELOE
from typing import List, Dict, Set
from sklearn.model_selection import train_test_split

class CarcinogenesisPredictor(object):
    """Estimate whether a compound is carcinogenic based on the $CELOE$ algorithm of $ontolearn$ library.

       Github link to ontolearn:"https://github.com/dice-group/Ontolearn#installation"
    """
    def __init__(self, input_turtle_file: str):
        super(CarcinogenesisPredictor, self).__init__()
        self.input_turtle_file = input_turtle_file
        self.converter = Converter(self.input_turtle_file)
        self.carcinogenesis_models: Dict[str, BaseConceptLearner] = dict()
        self.carcinogenesis_scores: Dict[str, Scores] = dict()
        self.true_positive_individuals: Dict[str, Set] = dict()
        self.true_negative_individuals: Dict[str, Set] = dict()
        self.predicted_positive_individuals: Dict[str, Set] = dict()
        self.predicted_negative_individuals: Dict[str, Set] = dict()
        self.learning_problems: List[str] = list()

    def fitAndPredict(self) -> Dict[str, Set]:
        print("Processing input...")
        input_json_file = self._process_input(self.input_turtle_file)
        print('Input processed')
    
        with open(input_json_file) as json_file:
            config = json.load(json_file)
        carcinogenesis_knowledge_base = KnowledgeBase(path=config['data_path'])

        for learning_problem, examples in config['problems'].items():
            positive_examples = examples['positive_examples']
            negative_examples = examples['negative_examples']

            self.learning_problems.append(learning_problem)

            print('Splitting data into train and test data for learning problem {0}...'.format(learning_problem))
            train_positive_examples, test_positive_examples = train_test_split(positive_examples, test_size=0.2, random_state=42)
            train_negative_examples, test_negative_examples = train_test_split(negative_examples, test_size=0.2, random_state=42)
            print('Split done')

            # Convert lists to sets
            train_positive_examples = set(train_positive_examples)
            test_positive_examples = set(test_positive_examples)
            train_negative_examples = set(train_negative_examples)
            test_negative_examples = set(test_negative_examples)

            print('Fitting for learning problem {0}...'.format(learning_problem))
            self.carcinogenesis_models[learning_problem] = CELOE(knowledge_base=carcinogenesis_knowledge_base, max_runtime=1)
            self.carcinogenesis_models[learning_problem].fit(pos=train_positive_examples, neg=train_negative_examples)
            print('Fit done')

            print('Predicting for learning problem {0}...'.format(learning_problem))
            self.true_positive_individuals[learning_problem] = test_positive_examples.copy()
            self.true_negative_individuals[learning_problem] = test_negative_examples.copy()

            test_individuals_list = test_positive_examples.union(test_negative_examples)
            
            # Get the best hypothesis
            hypothesis = self.carcinogenesis_models[learning_problem].best_hypotheses(n=1)
            
            # Obtain set of instances from a dataframe
            predictions_df = self.carcinogenesis_models[learning_problem].predict(individuals=test_individuals_list, hypotheses=hypothesis)
            predicted_instances_df = predictions_df[predictions_df.iloc[:,0] == 1]
            
            # Convert indices to strings in the required format
            instances = predicted_instances_df.index.to_list()
            str_instances: List[str] = list()
            for instance in instances:
                str_instances.append(str(instance).replace('carcinogenesis.','http://dl-learner.org/carcinogenesis#'))
            predicted_instances = set(str_instances)

            print('Prediction done')
            
            self.predicted_positive_individuals[learning_problem] = predicted_instances.copy()
            self.predicted_negative_individuals[learning_problem] = test_individuals_list.difference(predicted_instances).copy()
            
        print('Serializing output...')
        self._process_output()
        print('Serializing done')

        return self.predicted_positive_individuals

    def fit(self):
        print("Processing training input...")
        input_json_file = self._process_input(self.input_turtle_file)
        print('Training input processed')
        print('Fitting...')
        with open(input_json_file) as json_file:
            config = json.load(json_file)
        carcinogenesis_knowledge_base = KnowledgeBase(path=config['data_path'])

        for learning_problem, examples in config['problems'].items():
            positive_examples = set(examples['positive_examples'])
            negative_examples = set(examples['negative_examples'])

            self.learning_problems.append(learning_problem)

            self.carcinogenesis_models[learning_problem] = CELOE(knowledge_base=carcinogenesis_knowledge_base, max_runtime=1)
            self.carcinogenesis_models[learning_problem].fit(pos=positive_examples, neg=negative_examples)

        print('Fit done')
            
    def predict(self, test_input_turtle_file: str) -> Dict[str, Set]:
        print("Processing testing input...")
        test_json_file = self._process_input(test_input_turtle_file)
        print('Testing input processed')

        print('Predicting...')
        with open(test_json_file) as json_file:
            config = json.load(json_file)

        for learning_problem, examples in config['problems'].items():
            positive_examples = set(examples['positive_examples'])
            negative_examples = set(examples['negative_examples'])

            self.true_positive_individuals[learning_problem] = positive_examples.copy()
            self.true_negative_individuals[learning_problem] = negative_examples.copy()

            individuals_list = positive_examples.union(negative_examples)
            # Get the best hypothesis
            hypothesis = self.carcinogenesis_models[learning_problem].best_hypotheses(n=1)
            # Obtain set of instances from a dataframe
            predictions_df = self.carcinogenesis_models[learning_problem].predict(individuals=individuals_list, hypotheses=hypothesis)
            predicted_instances_df = predictions_df[predictions_df.iloc[:,0] == 1]
            
            # Convert indices to strings in the required format
            instances = predicted_instances_df.index.to_list()
            str_instances: List[str] = list()
            for instance in instances:
                str_instances.append(str(instance).replace('carcinogenesis.','http://dl-learner.org/carcinogenesis#'))
            predicted_instances = set(str_instances)
            

            self.predicted_positive_individuals[learning_problem] = predicted_instances.copy()
            self.predicted_negative_individuals[learning_problem] = individuals_list.difference(predicted_instances).copy()
            
        print('Prediction done')

        print('Serializing output...')
        self._process_output()
        print('Serializing done')

        return self.predicted_positive_individuals            

    def scores(self) -> Dict[str, Scores]:
        for learning_problem in self.learning_problems:
            self.carcinogenesis_scores[learning_problem] = Scores(self.true_positive_individuals[learning_problem], self.true_negative_individuals[learning_problem], self.predicted_positive_individuals[learning_problem])
            print('Scores for learning problem: ', learning_problem)
            self.carcinogenesis_scores[learning_problem].print_scores()

        self.macro_f1_score()
        return self.carcinogenesis_scores

    def macro_f1_score(self) -> float:
        sum_of_all_f1_scores = 0.0
        for learning_problem in self.learning_problems:
            sum_of_all_f1_scores += self.carcinogenesis_scores[learning_problem].f1_score

        macro_f1_score_value = sum_of_all_f1_scores / len(self.learning_problems)
        print('Macro F1 Score: ', macro_f1_score_value)
        return macro_f1_score_value

    def _process_input(self, input_turtle_file: str) -> str:
        return self.converter.ttl_to_json(input_turtle_file)

    def _process_output(self) -> str:
        return self.converter.serialize_ttl_using(self.learning_problems, self.predicted_positive_individuals, self.predicted_negative_individuals)