from CarcinogenesisPredictor import CarcinogenesisPredictor

# Provide input turtle file
cp = CarcinogenesisPredictor('../Resources/Input/kg-mini-project-train_v2.ttl')
cp.fitAndPredict()
# Obtain and print scores
cp.scores()

