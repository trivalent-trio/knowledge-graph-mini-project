from ontolearn.metrics import Recall, Precision, F1

class Scores(object):
    """This is a wrapper class for Recall, Precision, F1 scores in metrics of ontolearn.

    Github link to ontolearn:"https://github.com/dice-group/Ontolearn#installation" 
    """
    def __init__(self, positive_individuals, negative_individuals, predicted_instances):
        super(Scores, self).__init__()
        self.recall_score = Recall().score(positive_individuals, negative_individuals, predicted_instances)
        self.precision_score = Precision().score(positive_individuals, negative_individuals, predicted_instances)
        self.f1_score = F1().score(positive_individuals, negative_individuals, predicted_instances)

    def print_scores(self):
        print('Recall: ', self.recall_score)
        print('Precision: ', self.precision_score)
        print('F1: ', self.f1_score)