import os
import json
import shutil
from pathlib import Path
from typing import Dict, List
from rdflib import Graph, plugins, tools, URIRef, Literal, BNode, Namespace
from rdflib.parser import Parser
from rdflib.serializer import Serializer

class Converter(object):
	def __init__(self, input_turtle_file: str):
		super(Converter, self).__init__()
		self.file_path = input_turtle_file
		
	def serialize_ttl_using(self, learning_problems: List, predicted_positive_individuals: Dict, predicted_negative_individuals: Dict):
		g = Graph()

		CARCINOGENESIS = Namespace("http://dl-learner.org/carcinogenesis#")
		LPRES = Namespace("https://lpbenchgen.org/resource/")
		LPPROP = Namespace("https://lpbenchgen.org/property/")

		g.bind("carcinogenesis", CARCINOGENESIS)
		g.bind("lpres", LPRES)
		g.bind("lpprop", LPPROP)

		# Positive subjects list
		res1pos = LPRES.result_1pos
		res2pos = LPRES.result_2pos
		res3pos = LPRES.result_3pos
		res4pos = LPRES.result_4pos
		res5pos = LPRES.result_5pos
		res6pos = LPRES.result_6pos
		res7pos = LPRES.result_7pos
		res8pos = LPRES.result_8pos
		res9pos = LPRES.result_9pos
		res10pos = LPRES.result_10pos
		res11pos = LPRES.result_11pos
		res12pos = LPRES.result_12pos
		res13pos = LPRES.result_13pos
		res14pos = LPRES.result_14pos
		res15pos = LPRES.result_15pos
		res16pos = LPRES.result_16pos
		res17pos = LPRES.result_17pos
		res18pos = LPRES.result_18pos
		res19pos = LPRES.result_19pos
		res20pos = LPRES.result_20pos
		res21pos = LPRES.result_21pos
		res22pos = LPRES.result_22pos
		res23pos = LPRES.result_23pos
		res24pos = LPRES.result_24pos
		res25pos = LPRES.result_25pos

		result_varp_list = list()
		result_varp_list.append(res1pos)
		result_varp_list.append(res2pos)
		result_varp_list.append(res3pos)
		result_varp_list.append(res4pos)
		result_varp_list.append(res5pos)
		result_varp_list.append(res6pos)
		result_varp_list.append(res7pos)
		result_varp_list.append(res8pos)
		result_varp_list.append(res9pos)
		result_varp_list.append(res10pos)
		result_varp_list.append(res11pos)
		result_varp_list.append(res12pos)
		result_varp_list.append(res13pos)
		result_varp_list.append(res14pos)
		result_varp_list.append(res15pos)
		result_varp_list.append(res16pos)
		result_varp_list.append(res17pos)
		result_varp_list.append(res18pos)
		result_varp_list.append(res19pos)
		result_varp_list.append(res20pos)
		result_varp_list.append(res21pos)
		result_varp_list.append(res22pos)
		result_varp_list.append(res23pos)
		result_varp_list.append(res24pos)
		result_varp_list.append(res25pos)

		# Negative subjects list
		res1neg = LPRES.result_1neg
		res2neg = LPRES.result_2neg
		res3neg = LPRES.result_3neg
		res4neg = LPRES.result_4neg
		res5neg = LPRES.result_5neg
		res6neg = LPRES.result_6neg
		res7neg = LPRES.result_7neg
		res8neg = LPRES.result_8neg
		res9neg = LPRES.result_9neg
		res10neg = LPRES.result_10neg
		res11neg = LPRES.result_11neg
		res12neg = LPRES.result_12neg
		res13neg = LPRES.result_13neg
		res14neg = LPRES.result_14neg
		res15neg = LPRES.result_15neg
		res16neg = LPRES.result_16neg
		res17neg = LPRES.result_17neg
		res18neg = LPRES.result_18neg
		res19neg = LPRES.result_19neg
		res20neg = LPRES.result_20neg
		res21neg = LPRES.result_21neg
		res22neg = LPRES.result_22neg
		res23neg = LPRES.result_23neg
		res24neg = LPRES.result_24neg
		res25neg = LPRES.result_25neg

		result_varn_list = list()
		result_varn_list.append(res1neg)
		result_varn_list.append(res2neg)
		result_varn_list.append(res3neg)
		result_varn_list.append(res4neg)
		result_varn_list.append(res5neg)
		result_varn_list.append(res6neg)
		result_varn_list.append(res7neg)
		result_varn_list.append(res8neg)
		result_varn_list.append(res9neg)
		result_varn_list.append(res10neg)
		result_varn_list.append(res11neg)
		result_varn_list.append(res12neg)
		result_varn_list.append(res13neg)
		result_varn_list.append(res14neg)
		result_varn_list.append(res15neg)
		result_varn_list.append(res16neg)
		result_varn_list.append(res17neg)
		result_varn_list.append(res18neg)
		result_varn_list.append(res19neg)
		result_varn_list.append(res20neg)
		result_varn_list.append(res21neg)
		result_varn_list.append(res22neg)
		result_varn_list.append(res23neg)
		result_varn_list.append(res24neg)
		result_varn_list.append(res25neg)

		for i in range(len(learning_problems)):
			lp = learning_problems[i]

			# Positive Individuals
			pos_result_var = result_varp_list[i]

			g.add((pos_result_var, LPPROP.pertainsTo, URIRef(lp)))
			g.add((pos_result_var, LPPROP.belongsToLP, Literal(bool(True))))
			for individuals in predicted_positive_individuals[lp]:
				g.add((pos_result_var, LPPROP.resource, URIRef(individuals)))
			
			# Negative Individuals
			neg_result_var = result_varn_list[i]

			g.add((neg_result_var, LPPROP.pertainsTo, URIRef(lp)))
			g.add((neg_result_var, LPPROP.belongsToLP, Literal(bool(False))))
			for individuals in predicted_negative_individuals[lp]:
				g.add((neg_result_var, LPPROP.resource, URIRef(individuals)))

		p = Path(self.file_path)
		result_ttl_file = os.path.join(os.path.dirname(p), '..', '..', 'Output', 'classification_result.ttl')
		g.serialize(destination=result_ttl_file, format='ttl', indent=4)

	def ttl_to_json(self, file_path:str) -> str:
		self.file_path = file_path
		# Convert to intermediate json-ld file
		intermediate_json_ld_file = self._ttl_to_json_ld(file_path)
		# Convert to json file for input to ontolearn
		json_file = self._json_ld_to_json(intermediate_json_ld_file)
		# os.remove(intermediate_json_ld_file)

		return json_file

	def _ttl_to_json_ld(self, file_path:str) -> str:
		gr=Graph()
		gr.parse(file_path,format='ttl')
		p = Path(file_path)
		intermediate_json_ld_file = os.path.join(os.path.dirname(p),'..', 'Intermediate', os.path.splitext(os.path.basename(p))[0] + '.json-ld')
		gr.serialize(destination=intermediate_json_ld_file, format='json-ld', indent=4)

		return(intermediate_json_ld_file)

	def _json_ld_to_json(self, file_path:str) -> str:
		# Output json
		carcin_input_json_dict = dict()
		# OWL File Path
		carcin_input_json_dict['data_path'] = '../Resources/Carcinogenesis/carcinogenesis.owl'
		
		# Read from json-ld file
		with open(file_path) as f:
			data = json.load(f)
		
		problems_dict: Dict[str,Dict[str,Dict[str, List]]] = dict()
		lp_dict: Dict[str,Dict[str, List]] = dict()
		
		for element in data:
			examples_dict: Dict[str, List] = dict()
			individuals = list()
		
			# Positive examples list
			for individual in element['https://lpbenchgen.org/property/includesResource']:
				individuals.append(individual['@id'])
		
			examples_dict['positive_examples'] = individuals.copy()
			individuals.clear()
		
			# Negative examples list
			for individual in element['https://lpbenchgen.org/property/excludesResource']:
				individuals.append(individual['@id'])
		
			examples_dict['negative_examples'] = individuals.copy()
			individuals.clear()
		
			lp_dict[element['@id']] = examples_dict
		
		carcin_input_json_dict['problems'] = lp_dict
		
		p = Path(file_path)
		# Write the output json
		json_file = os.path.join(os.path.dirname(p),'..', 'Intermediate', os.path.splitext(os.path.basename(p))[0] + '.json')
		with open(json_file, 'w') as input_json_file:
		  json.dump(carcin_input_json_dict, input_json_file)

		return json_file
		
		