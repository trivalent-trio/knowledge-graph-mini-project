# Knowledge Graph Mini Project

This project aims to classify the chemical compounds from Carcinogenesis dataset as carcinogenic or not in the context of multiple learning problems.
This project is based on the python library Ontolearn (https://github.com/dice-group/Ontolearn). Ontolearn is an open-source software library for explainable structured machine learning in Python.
This project is created as per the requirements of the course 'Foundation of Knowledge Graphs' at Paderborn University.

This program accepts input file(s) in the turtle format (.ttl - https://www.w3.org/TR/turtle/).
The input file(s) must be present in the `Resources/Input` directory of the project.
After execution, the output file can be found in the `Output` directory of the project. The output file will also be created in the turtle format (.ttl - https://www.w3.org/TR/turtle/)
Moreover, scores related to the estimation process like Recall, Precision, F1-score and Macro F1-score can also be obtained.

The project structure has 3 main directories as follows:
1. `Code`: The directory for executing the program; contains the code for the program to execute and dependencies (ontolearn)
2. `Resources`: Contains the Resources (excluding code) required to run the program (e.g. all the input and intermediate files in the formats .ttl, .owl, .json, .json-ld etc.)  
	Has three subdirectories:  
		a. `Input`: For placing the input .ttl file(s)  
		b. `Intermediate` (Used internally by program): For intermediate files created during conversion from .ttl to .json  
		c. `Carcinogenesis` (Used internally by program): For the knowledge base of Carcinogenesis dataset; contains carcinogenesis.owl  
3. `Output`: Contains the output file `classification_result.ttl` (after execution of the program)
	
# Installation
 This program requires a Python environment for its installation and execution.
 1. Clone the project repository.
 ```
 # Using SSH
 git clone git@gitlab.com:trivalent-trio/knowledge-graph-mini-project.git
 
 # OR
 
 # Using HTTPS
 git clone https://gitlab.com/trivalent-trio/knowledge-graph-mini-project.git
 ```
 
 2. Install the requirements.
 ```
 pip install -r requirements.txt
 ```

# Usage
The usage of the program depends on whether the user has split the data into train and test data or not.  
If the data has not been split, then follow the steps for a single input file.  
In case, the data has been split into train and test data, then follow the steps for two input files. 

There are two ways in which this program can be used:  
1. Single input file:  
	The program automatically splits the input data into train and test data for its execution.  
	Only a single input file in the turtle (.ttl) format is placed in the `Resources/Input` directory.

	The program can be executed using single input file as follows:   
	a. Place the input file containing various learning problems based on Carcinogenesis dataset in the `Resources/Input` directory. (A sample file `kg-mini-project-train_v2.ttl` is present in the `Resources/Input` for reference)   
	b. Change your directory to `Code` directory in the project
	```
	cd Code
	```
	c. Execute the program as follows:
	```
	from CarcinogenesisPredictor import CarcinogenesisPredictor

	# Provide input turtle file
	cp = CarcinogenesisPredictor('../Resources/Input/kg-mini-project-train_v2.ttl')
	cp.fitAndPredict()
	# Obtain and print scores
	cp.scores()
	```
	OR  
	Simply execute the sample file `example_single_input_file.py` as follows:
	```
	python example_single_input_file.py 
	```
	d. The classification result file (`classification_result.ttl`) can be found in the `Output` directory of the project.

2. Two input files:  
	The data is already separated by the user into train and test data into two separate turtle files (.ttl) for input to the program.  
	Two input files (a training file and a testing file) in the turtle (.ttl) format are placed in the `Resources/Input` directory.  
	The program can be executed using two input files as follows:  
	a. Place the train and test input files containing various learning problems based on Carcinogenesis dataset in the `Resources/Input` directory. (Sample files `kg-mini-project-grading.ttl` and `kg-mini-project-testing.ttl` are present in the `Resources/Input` for reference)  
	b. Change your directory to `Code` directory in the project
	```
	cd Code
	```
	c. Execute the program as follows:
	```
	from CarcinogenesisPredictor import CarcinogenesisPredictor
	
	# Provide training data turtle file based on Carcinogenesis dataset
	cp = CarcinogenesisPredictor('../Resources/Input/kg-mini-project-grading.ttl')
	cp.fit()
	# Provide testing data turtle file based on Carcinogenesis dataset
	cp.predict('../Resources/Input/kg-mini-project-testing.ttl')
	cp.scores()
	```
	OR  
	Simply execute the sample file `example_train_test_input_files.py` as follows:
	```
	python example_train_test_input_files.py 
	```
	d. The classification result file (`classification_result.ttl`) can be found in the `Output` directory of the project.
	
Note: (An empirical observation) The execution time should not (usually) exceed 6 minutes for an input file of size ~14 MB.

For any further questions, please contact: `gpandit@mail.uni-paderborn.de`, `nalanda@campus.uni-paderborn.de`, `barunk@campus.uni-paderborn.de`
